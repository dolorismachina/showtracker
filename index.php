<?php
	$pageTitle = "Welcome to ShowTracker";
	
	include 'templateheader.php';
	
	if (loggedIn())
		header('Location: profile.php');
	else if (!loggedIn())
	{
?>
<article>
	<header>
		Login
	</header>
	<form action = "login.php" method = "post">
		<table>
			<tr>
				<td><label for = "username">Username: </label></td>
			</tr>
			<tr>
				<td><input type = "text" name = "username" class = "text" id = "username" value = "" /></td>
			</tr>
			<tr>
				<td><label for = "password">Password</label></td>
			</tr>
			<tr>
				<td><input type = "password" name = "password" class = "text" id = "password" value = "" /></td>
			</tr>
			<tr>
				<td colspan = "2"><input type = "submit" value = "login" class = "submit" id = "login" /></td>
			</tr>	
		</table>
	</form>
</article>

<article>
	<p>
		Don't have an account?
	</p> 
	<p>
		<a href = "register.php">Register Now!</a>
	</p>
</article>
<?php
	}
	else
	{
?>
<p>
	Welcome <?php echo $_SESSION['username']; ?>
</p>
<?php
	}
	include 'templatefooter.php';
?>