<?php
	include 'config.php';
	
	$user;
	$loggedIn;

	// Check if logged in
	if (!isset($_SESSION['username']))
	{
		$loggedIn = false;
	}
	else
	{
		$loggedIn = true;
		$user = $_SESSION['username'];
	}
?>

<!DOCTYPE html>
<html lang = "en">
<head>
	<title><?php echo $pageTitle; ?></title>
	<meta charset = "utf-8" />
	<link rel = "stylesheet" type = "text/css" href = "style.css" />
	<link rel = "icon" type = "image/png" href = "s.png" />
</head>

<body>
<div id = "mainDiv">
	<header id = "topHeader">
		<a href = "index.php">Show Tracker</a>
	</header>
	
	<?php
		if (loggedIn())
		{
	?>
	<nav id = "topNavigation">
		<a href = "logout.php">Logout</a>
	</nav>
	<?php
		}
	?>
<section id = "mainSection">