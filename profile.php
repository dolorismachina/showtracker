<?php
	$pageTitle = "Profile";
	
	include 'templateheader.php';
	
	if (!loggedIn())
		header('Location: index.php');
?>
<article>
	<header>
		List of shows
	</header>
	<form action = "updateshows.php" method = "post">
		<table id = "shows">
			<?php
				$shows = $db->query("SELECT showname, episodes FROM user_shows WHERE username = '".$_SESSION['username']."'");
				for ($i = 0; $i < $shows->num_rows; $i++)
				{
					$row = $shows->fetch_assoc();
			?>
			<tr>
				<input type = "hidden" name = "<?php echo 'show'.$i; ?>" value = "<?php echo $row['showname']; ?>" />	
			</tr>
			<tr>
				<td>
					<div class = "showName">
					<label for = "<?php echo 'show'.$i ?>"><?php echo $row['showname']; ?></label>
					<a href = "<?php echo 'removeShow.php?showname='.$row['showname'] ?>" class = "removeShow" title = "Are you sure you want to remove this show?">remove</a>
					</div>
				</td>
			</tr>
			<tr>
				<td><input type = "text" name = "<?php echo 'episode'.$i; ?>" value = "<?php echo intval($row['episodes']);?>" class = "text" id = "<?php echo 'show'.$i ?>"/></td>
			</tr>
			<?php
				} // Closing for loop
			?>
			<tr>
				<td colspan = "2"><input type = "submit" value = "Update" class = "submit" id = "updateShows"/></td>
			</tr>
		</table>
	</form>
</article>

<article>
	<header>
		Add another show
	</header>
	
	<form action = "addshow.php" method = "post">
		<table>
			<tr>
				<td><label for = "show">Show</label></td>
			</tr>
			<tr>
				<td><input type = "text" name = "show" id = "show" /></td>
			</tr>
			<tr>
				<td><label for = "episodes">Episodes</label></td>
			</tr>
			<tr>
				<td><input type = "number" name = "episodes" id = "episodes" /></td>
			</tr>
			<tr>
				<td><input type = "submit" class = "submit" id = "addShow" /></td>
			</tr>
		</table>
	</form>
</article>
<?php
include 'templatefooter.php';
?>