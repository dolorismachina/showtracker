<?php
	// Establishing a connection to the database
	$db = new mysqli('localhost', 'webuser', 'webuserpassword', 'showtracker');
	
	session_start();
	
	function loggedIn()
	{	
		if (!isset($_SESSION['username']))
			return false;
		else
			return true;
	}
	
	function sanitiseString($s)
	{
		$s = strip_tags($s);
		$s = htmlentities($s);
		$s = stripslashes($s);
		return mysql_real_escape_string($s);
	}
?>